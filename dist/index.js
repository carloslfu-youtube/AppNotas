// Referencias al DOM
var editorElm = document.getElementById('editor');
var anteriorElm = document.getElementById('anterior');
var siguienteElm = document.getElementById('siguiente');
var eliminarElm = document.getElementById('eliminar');
var guardarElm = document.getElementById('guardar');
var nuevaElm = document.getElementById('nueva');
// Estado
var notas = [''];
var notaActual = 0;
// Logica
function guardar(texto) {
    notas[notaActual] = texto;
}
function nueva() {
    notaActual = notas.length;
    notas[notaActual] = '';
}
function eliminar() {
    // siempre debe haber una nota guardada (por defecto)
    if (notaActual === 0) {
        notas[0] = '';
    }
    else {
        notas.splice(notaActual, 1);
    }
    if (notaActual === notas.length) {
        notaActual = notas.length - 1;
    }
}
function anterior() {
    if (notaActual !== 0) {
        notaActual--;
    }
}
function siguiente() {
    if (notaActual !== notas.length - 1) {
        notaActual++;
    }
}
// Comunicacion (I/O): Escuchadores de eventos y otros efectos laterales
guardarElm.addEventListener('click', function () {
    guardar(editorElm.value);
    console.log(notas);
    console.log(notaActual);
});
nuevaElm.addEventListener('click', function () {
    nueva();
    renderEditor();
});
anteriorElm.addEventListener('click', function () {
    anterior();
    renderEditor();
});
siguienteElm.addEventListener('click', function () {
    siguiente();
    renderEditor();
});
eliminarElm.addEventListener('click', function () {
    eliminar();
    renderEditor();
});
// renderizadores: colocan el estado en el DOM
function renderEditor() {
    editorElm.value = notas[notaActual];
}
//# sourceMappingURL=index.js.map