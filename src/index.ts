
// Referencias al DOM
let editorElm = <HTMLTextAreaElement> document.getElementById('editor')
let anteriorElm = document.getElementById('anterior')
let siguienteElm = document.getElementById('siguiente')
let eliminarElm = document.getElementById('eliminar')
let guardarElm = document.getElementById('guardar')
let nuevaElm = document.getElementById('nueva')

// Estado

let notas = ['']
let notaActual = 0

// Logica

function guardar (texto: string) {
  notas[notaActual] = texto
}

function nueva () {
  notaActual = notas.length
  notas[notaActual] = ''
}

function eliminar () {
  // siempre debe haber una nota guardada (por defecto)
  if (notaActual === 0) {
    notas[0] = ''
  } else {
    // elimina la nota actual
    notas.splice(notaActual, 1)
  }
  if (notaActual === notas.length) {
    notaActual = notas.length - 1
  }
}

function anterior () {
  if (notaActual !== 0) {
    notaActual--
  }
}

function siguiente () {
  if (notaActual !== notas.length - 1) {
    notaActual++
  }
}

// Comunicacion (I/O): Escuchadores de eventos y otros efectos laterales

guardarElm.addEventListener('click', () => {
  guardar(editorElm.value)
  console.log(notas)
  console.log(notaActual)
})

nuevaElm.addEventListener('click', () => {
  nueva()
  renderEditor()
})

anteriorElm.addEventListener('click', () => {
  anterior()
  renderEditor()
})

siguienteElm.addEventListener('click', () => {
  siguiente()
  renderEditor()
})

eliminarElm.addEventListener('click', () => {
  eliminar()
  renderEditor()
})

// renderizadores: colocan el estado en el DOM, mapeo estado -> vista
function renderEditor() {
  editorElm.value = notas[notaActual]
}
